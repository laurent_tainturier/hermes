import requests
import yaml

with open("config.yaml") as config_file:
    config = yaml.safe_load(config_file)

if __name__ == "__main__":

    with open("dossiers_ids.txt", 'r') as dossier_content:
        camunda_config = config['camunda']
        url = camunda_config['remise-au-domaine']

        for line in dossier_content.readlines():
            if line[0] != "#":
                value = line.split(' ')[-1].replace("\n", "")

                url_req = url + value
                print(url_req)

                r = requests.post(url_req)

                print(r.status_code)
                print(r.text)
